<?php
require "Head.php";
require "Body.php";

class View {
    // Définition des attributs
    public $head;
    public $body;

    // Définition du constructeur
    function __construct() {
        $this->head = new Head();  
        $this->body = new Body();          
    }

    // Définition des méthodes
    function render() {
        $html = 
            "<!DOCTYPE html>"
            ."<html>"
            .$this->head->render()
            .$this->body->render()
            ."</html>"
        ; 
        $view_file = fopen("./src/view/result.html", "w");
        fwrite($view_file, $html);
        fclose($view_file);     
        echo $html;
    }

    // Méthodes relatives à la tête de page
    function hasTitle($s) {
        $this->head->hasTitle($s);
    }

    function hasStyle($s) {
        $this->head->hasStyle($s);
    }

    // Méthodes relatives au corps de page
    function hasNav($s) {
        $this->body->hasNav($s);
    }
    
    function hasHeader($s) {
        $this->body->hasHeader($s);
    }

    function hasFooter($s) {
        $this->body->hasFooter($s);
    }

    function hasContent($s) {
        $this->body->hasContent($s);
    }
}
?>