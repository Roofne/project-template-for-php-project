<?php
class Head {
    // Définition des attributs
    private $title;
    private $style;

    // Définition du constructeurs
    function __construct() {

    }
 
    // Définition des méthodes
    function render() {
        $html = "
            <head>"
            .'<title>'.$this->title."</title>"
            ."<link rel='stylesheet' href='src/view/css/".$this->style."'>"
            ."</head>
        ";
        return $html;
    }

    function hasTitle($_title) {
        $this->title = $_title;
    }
    
    function hasStyle($_style) {
        $this->style = $_style;
    }
}
?>
