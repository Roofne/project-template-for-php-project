<?php
require "View.php";

class ViewBurger extends View {
    // Définition du constructeur
    function __construct() {
        parent::__construct();
        $this->hasTitle("Burger World");
        $this->hasStyle("main.css");
        #$this->hasStyle("burger.css");
        $this->hasHeader("<h1>Welcome to Burger World</h1>");
        $this->hasContent("<div class='box'>lorem</div>");
        $this->hasContent("<div class='box'>lorem</div>");
        $this->hasFooter("Toine VINTEL - 2022");
    }

    // Définition des méthodes
    function hasBurgerList($s) {
        $this->body->content .= "<table>";
        foreach ($s as $row) {
            $this->body->content .= "<tr>";
            $this->body->content .= "<td>".$row['name']."</td>";
            $this->body->content .= "<td>".$row['price']."</td>";
            $this->body->content .= "</tr>";
        }
        $this->body->content .= "</table>";
    }
}
?>