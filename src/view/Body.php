<?php
class Body {
    // Définition des attributs
    public $nav;
    public $header;
    public $footer;
    public $content;

    // Définition du constructeurs
    function __construct() {
        $this->nav = "";
        $this->header = "";
        $this->footer = "";
        $this->content = "";
    }
 
    // Définition des méthodes
    function render() {
        $html = 
            "<body>"
            .'<header>'.$this->header."</header>"
            .'<nav>'.$this->nav."</nav>"
            .$this->content
            .'<footer>'.$this->footer."</footer>"
            ."</body>
        ";
        return $html;
    }

    function hasNav($_nav) {
        $this->nav = $_nav;
    }
    
    function hasHeader($_header) {
        $this->header = $_header;
    }

    function hasFooter($_footer) {
        $this->footer = $_footer;
    }

    function hasContent($_content) {
        $this->content .= $_content;
    }
}
?>