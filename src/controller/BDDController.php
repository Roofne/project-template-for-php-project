<?php
class BDDController {
    // Définition des attributs
    private $instance;
    private $db_driver;
    private $db_host;
    private $db_name;
    private $db_user;
    private $db_pwd;

    // Définition du constructeur
    function __construct($args) {
        $this->db_driver = isset($args["driver"]) ? $args["driver"] : null;
        $this->db_host   = isset($args["host"])   ? $args["host"]   : null;
        $this->db_name   = isset($args["name"])   ? $args["name"]   : null;
        $this->db_user   = isset($args["user"])   ? $args["user"]   : null;
        $this->db_pwd    = isset($args["pwd"])    ? $args["pwd"]    : null;
    }

    // Définition des getters et setters    

    // Définition des méthodes

    // Définition des méthodes de connections
    function connect() {
        switch ($this->db_driver) {
            case "mysql":
                echo_console("Connecting to MySQL");
                $this->connect_mysql();
                break;
            case "oci":
                echo_console("Connecting to Oracle");
                $this->connect_oracle();
                break;
            default:
                echo_console("Error: Invalid Driver Name");
                break;
        }
    }

    function connect_mysql() {  
        try {
            echo_console($this->toString());
            $this->instance = new PDO(
                $this->db_driver.":host=".$this->db_host.";dbname=".$this->db_name,
                $this->db_user,
                $this->db_pwd
            );
            $this->instance->query("SET NAMES 'utf8'");
            $this->instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo_console("Connected to MySQL Database");
        } catch(PDOException $e) {
            echo_console('Error: Cannot connect to the MySQL Database');
            echo_console($e);
        }
      }
    
    function connect_oracle() {
        try {
            $port = "1521";
            $lien_base =
                "oci:dbname=(DESCRIPTION =
                    (ADDRESS_LIST =
                    (ADDRESS =
                        (PROTOCOL = TCP)
                        (Host = ".$db_host .")
                        (Port = ".$port."))
                    )
                    (CONNECT_DATA =
                    (SERVICE_NAME = ".$db_name.")
                )
            )";
            $this->instance = new PDO($lien_base,$db_user,$db_pwd);
            $this->instance->query("SET NAMES 'utf8'");
            $this->instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo_console("Connected to the Oracle Database");
        } catch(PDOException $e) {
            echo_console('Error: Cannot connect to the Oracle Database');
        }
    }

    // Définition des méthodes de requètes
    function query($cmd) {
        try {
            $temp = $this->instance->query($cmd);
            return $temp;    
        } catch(PDOException $e) {
            echo_console($e);
        }    
    }

    // Définition des méthodes de déconnection
    function disconnect() {
        $this->instance = null;
    }  

    // Autres méthodes
    function toString() {
        return $this->db_driver.":".$this->db_host." ".$this->db_name." ".$this->db_user." ".$this->db_pwd;;
    }
}
?>