<?php
class BurgerController {
    // Définition des attributs

    // Définition du constructeur
    function __construct() {

    }

    // Définition des getters et setters

    // Définition des methodes

    // Définition des méthodes ADD
    function add($c,$burger) {
        try {
            $c->query("insert into burger values(".$burger->getId().",'".$burger->getName()."',".$burger->getPrice().")");
        } catch(Exception $e) {
            echo_console($e);
        }
    }

    // Définition des méthodes GET
    function get($c,$i) {
        $data = null;
        try {
            $data = $c->query("select * from burger where id=".$i);
        } catch(Exception $e) {
            echo_console($e);
        }
        return $data;
    }

    function getAll($c) {
        $data = null;
        try {
            $data = $c->query("select * from burger");
        } catch(Exception $e) {
            echo_console($e);
        }
        return $data;
    }

    // Définition des méthodes UPDATE
    function update($c,$i, $burger) {
        try {
            $c->query("update burger set name = '".$burger->getName()."' where id=".$i);
        } catch(Exception $e) {
            echo_console($e);
        }
    }

    // Définition des méthodes DELETE
    function delete($c,$i) {
        try {
            $c->query("delete from burger where id=".$i);
        } catch(Exception $e) {
            echo_console($e);
        }
    }
}
?>