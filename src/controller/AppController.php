<?php
// Libraries
include "src/function/echo_console.php";

$controllers = ["BDD","Burger"];
$controller_default = "BDD";

function load_controller($name) {
    $filename = 'src/controller/'. $name .'Controller.php';
    if(file_exists($filename)){
        require_once $filename;
    } else {
        die("Error: Controller not found!");
    }
}

function load_model($name) {
    $filename = 'src/model/'. $name .'.php';
    if(file_exists($filename)){
        require_once $filename;
    } else {
        die("Error: Model not found!");
    }
}

function render_page($data) {
    // include "src/view/base/page-start.php";
    // include "src/view/base/page-end.php";
    foreach($data as $burger){
        echo $burger['id']."\t";
        echo $burger['name']."\t";
        echo $burger['price'];
        echo'<br>';
    }
}

echo_console("Loading App");

// Connection à la BDD
load_controller("BDD");
$json = array("driver"=>"mysql","host"=>"127.0.1.1:3306","user"=>"root","pwd"=>"TPLa9c5!claudie","name"=>"test");
$c = new BDDController($json);
$c->connect();

// Requète à la BDD
load_controller("Burger"); load_model("Burger");
$b = new BurgerController();
#$b->add($c,new Burger(2,'mac tonight',12.0));
#$b->delete($c,0);
#$b->update($c,1,new Burger(1,'mac tomorrow',13.0));
$data = $b->getAll($c);

// Creation de la vue
require "src/view/ViewBurger.php";
$v = new ViewBurger();
$v->hasBurgerList($data);
$v->render();

echo_console("App Loaded");
?>