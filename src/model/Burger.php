<?php
class Burger {
    // Définition des attributs
    private $id;
    private $name;
    private $price;

    // Définition du constructeur
    function __construct($id, $name, $price) {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }

    // Définition des getters et setters
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getPrice() {
        return $this->price;
    }

    // Définition des méthodes
}